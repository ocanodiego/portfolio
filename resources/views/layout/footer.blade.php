<footer class="container-fluid bg-dark">
	<div class="row px-2 py-4">
		<div class="col mt-3 text-center position-relative">
			<p class="white">info@poncianodiego.com</p>
			<p class="white mt-3">&copy; @php echo date("Y"); @endphp</p>
			<a class="position-absolute" style="top: 0; right: 10px;" href="https://www.upwork.com/fl/diegoponciano">
				<img style="width: 60px;" src="/img/upwork-logo.png" alt="Hire me on Upwork">
			</a>
		</div>
	</div>
</footer>