<nav class="navbar navbar-expand-lg navbar-light sticky-top bg-white">
  <a class="navbar-brand" href="/"><h1>Diego Ponciano</h1></a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#mainNavBar" aria-controls="mainNavBar" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="mainNavBar">
    <ul class="navbar-nav ml-auto">
      <li class="nav-item @if($name == 'home') active @endif">
        <a class="nav-link" href="/">Home</a>
      </li>
      <li class="nav-item @if($name == 'portfolio') active @endif">
        <a class="nav-link" href="/portfolio">Portfolio</a>
      </li>
      <li class="nav-item @if($name == 'resume') active @endif">
        <a class="nav-link" href="/resume">Resume</a>
      </li>
      <li class="nav-item @if($name == 'services') active @endif">
        <a class="nav-link" href="/services">Services</a>
      </li>
    </ul>
  </div>
</nav>
