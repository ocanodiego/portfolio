<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Diego Ponciano - Helping You Succeed On Your Next Software Project</title>
	<meta name="description" content="You are close to getting the help you need for your next web development project. You will be able to rely on more than 7 years of intense and meaningful experience and use them to your advantage and project success. The best Laravel and Node.js development at your fingertips." />

	<meta name="keywords" content="Laravel Development, Custom Software, Custom Web Application, Laravel dedicated companies, Laravel Development Companies, Node.js custom development, Vue.js custom development">

	<link rel="canonical" href="https://poncianodiego.com/">

	<meta property="og:title" content="Software Developer Diego Ponciano" />
	<meta property="og:type" content="website" />
	<meta property="og:url" content="https://www.poncianodiego.com/" />
	<meta property="og:image" content="http://poncianodiego.com/assets/logo-og.jpg" />
	<meta property="og:description" content="You are close to getting the help you need for your next web development project. You will be able to rely on more than 7 years of intense and meaningful experience and use them to your advantage and project success. The best Laravel and Node.js development at your fingertips." />

	<meta name="twitter:card" content="summary">
	<meta name="twitter:description" content="You are close to getting the help you need for your next web development project. You will be able to rely on more than 7 years of intense and meaningful experience and use them to your advantage and project success. The best Laravel and Node.js development at your fingertips.">
	<meta name="twitter:title" content="Diego Ponciano: Helping You Succeed On Your Next Web Development Project!">

	<script type="application/ld+json">{"@context":"https:\/\/schema.org","@type":"WebSite","@id":"#website","url":"https:\/\/poncianodiego.com\/","name":"Diego Ponciano","potentialAction":{"@type":"SearchAction","target":"https:\/\/poncianodiego.com\/?s={search_term_string}","query-input":"required name=search_term_string"}}</script>

	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-83490804-5"></script>
	<script>
	  window.dataLayer = window.dataLayer || [];
	  function gtag(){dataLayer.push(arguments);}
	  gtag('js', new Date());

	  gtag('config', 'UA-83490804-5');
	</script>

	<link href="https://fonts.googleapis.com/css?family=Lobster|Playfair+Display|Raleway" rel="stylesheet">
	<link rel="stylesheet" href="/css/bootstrap.min.css">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
	<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/lightgallery/1.6.12/css/lightgallery.css"/>
	<link rel="stylesheet" href="/css/app.css">
	@stack('styles')
</head>
<body>

@include('layout.header')

@yield('content')

@include('layout.footer')

<script src="/js/jquery.js"></script>
<script src="/js/popper.min.js"></script>
<script src="/js/bootstrap.min.js"></script>

@stack('scripts')

</body>
</html>