<div class="row mt-5">
  <div class="col-8 col-md-3 mx-auto mb-5">
    <img class="img-fluid me-pic" src="/img/me.jpg" alt="">
  </div>
  <div class="col-md-9 my-auto pl-4">
    <h2>That's me!</h2>
    <p class="mb-5">I consider myself to be an interdisciplinary Senior Software Developer with experience in many fields of the development process. I can help you succeed in your next software development project by capitalizing on my extensive experience in many different projects of all sizes and tech stacks. Feel free to contact me about your project!</p>
  </div>
</div>
