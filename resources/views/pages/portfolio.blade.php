@extends('layout.app')

@section('content')

<style>
	.embed-container { 
		position: relative; 
		padding-bottom: 56.25%; 
		height: 0; 
		overflow: hidden; 
		max-width: 100%;
	} 

	.embed-container iframe, .embed-container object, .embed-container embed { 
		position: absolute;
		top: 0;
		left: 0;
		width: 100%;
		height: 100%; 
	}

	.btn.btn-dark{
	    color: #ffffff;
	    background-color: #202020;
	    border-color: #202020;
	}

	.btn.btn-dark:hover{
	    background-color: #ccc;
	    border-color: #ccc;
	}
</style>

<div class="container mt-5">
	<div class="row">
		<div class="col-md-12 text-center mb-2">
			<h1>PORTFOLIO</h1>
			<!-- <p class="d-none d-sm-block">Check some of the projects I've worked with:</p> -->
			<h2 class="d-block fs-24">Check some of the projects I've worked with:</h2>
			<!-- <p>"With the right setup everything is possible"</p> -->
		</div>
	</div>
	<div class="row">
		@foreach($partners as $partner)
			@if($partner->projects()->count())
				<div class="col-12 partner-row mb-4">
					<div class="row justify-content-md-center">
						<div class="col-12 text-center">
							<h2>		
								@if(!$partner->isNone()) In collaboration with: 
								<a href="{{$partner->site_url}}" target="_blank">{{$partner->name}}</a>
								@endif
							</h2>
						</div>
						@foreach($partner->projects()->published()->orderByDesc('updated_at')->get() as $project)
							<div class="col-md-6 portfolio-item mt-4">
								<div class="frame" style="border: 1px solid; padding: 15px; border-radius: 10px;">
									<div class="content">
										<img class="img-fluid w-100" src="{{$project->public_thumbnail}}" alt="">
										<div class="overlay open-modal text-center d-none d-sm-flex" data-target="#{{$project->slug}}-modal">
											<a target="_blank" href="{{$project->internal_url}}" class="project-title">{{$project->short_name}} <br><br><i style="font-size: 24px;" class="fas fa-external-link-alt"></i></a>
										</div>
									</div>
									<p class="mt-4 d-block d-sm-none" style="font-size: 20px;" target="_blank" href="{{$project->internal_url}}">{{$project->short_name}}</p>
									<a class="btn btn-gold btn-lg text-center d-block d-sm-none" style="font-size: 20px;" href="{{$project->internal_url}}">See case</a>
								</div>
							</div>
						@endforeach
					</div>
				</div>
			@endif
		@endforeach

<!-- 		<div class="col-sm-8 mx-auto mt-5">
			<div class="embed-container">
				<iframe src='https://www.youtube.com/embed/kDnkTGUc-yQ' frameborder='0' allowfullscreen></iframe>
			</div>
		</div> -->
	
		<div class="col-sm-12 text-center mt-5">

			<a href="/services" style="font-size: 24px;">SERVICES <i class="fas fa-arrow-circle-right"></i></a>
		</div>
	</div>
	@include('layout.thatsme')
</div>

@endsection
