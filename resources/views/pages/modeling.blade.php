@extends('layout.app')

@section('content')

<style>
	.embed-container { 
		position: relative; 
		padding-bottom: 56.25%; 
		height: 0; 
		overflow: hidden; 
		max-width: 100%;
	} 

	.embed-container iframe, .embed-container object, .embed-container embed { 
		position: absolute;
		top: 0;
		left: 0;
		width: 100%;
		height: 100%; 
	}
</style>

<div class="container mt-5">
	<div class="row">
		<div class="col-12 mb-4">
			<div class="row justify-content-md-center">
				<div id="lightgallery" class="text-center">
					@foreach($photos as $photo)
						<a href="storage/{{ $photo->url }}">
							<img style="max-width: 300px; width: 100%;" src="storage/{{ $photo->url }}" />
						</a>
					@endforeach
				</div>
			</div>
		</div>
	</div>
</div>

@endsection

@push('scripts')

	<script>

		$('#lightgallery').lightGallery();

		$(".open-modal").on('click', function(){
			var target = $(this).data('target');
			$(target).modal('show');
			$(this).addClass('active');
		});

		$(".modal .close").on('click', function(){
			setTimeout(function(){
				$(".overlay").removeClass('active');
			}, 300);
		});

		$(".photo-show").slick();

		$('.modal').on('shown.bs.modal', function (e) {
		  $(this).find(".photo-show").slick('setPosition');
		})

	</script>

@endpush