@extends('layout.app')

@section('content')

<div class="container">
	<div class="row" style="min-height: 100vh;">
		<div class="col-md-6 mt-6">
		  	<div id="typed-strings1" style="display: none;">
		      <span>Need a hand with<br><strong>software development</strong>?</span>
		      <span>^500Ping me for a quote.</span>
		    </div>
		  	<div id="typed-strings2" style="display: none;">
		      	<span>
		      		<a target="_blank" href="mailto:jay.dee.force@gmail.com?subject=Hello I need help with a React Project">&middot; React</a> <br>
		      		<a target="_blank" href="mailto:jay.dee.force@gmail.com?subject=Hello I need help with a Django Project">&middot; Django</a> <br>
		      		<a target="_blank" href="mailto:jay.dee.force@gmail.com?subject=Hello I need help with a Laravel Project">&middot; Laravel</a> <br>
		      		<a target="_blank" href="mailto:jay.dee.force@gmail.com?subject=Hello I need help with a Vue.js Project">&middot; Vue.js</a> <br>
		      		<a target="_blank" href="mailto:jay.dee.force@gmail.com?subject=Hello I need help with a Node.js Project">&middot; Node.js</a> <br>
		      		<a target="_blank" href="mailto:jay.dee.force@gmail.com?subject=Hello I need help with a React Native Project">&middot; React Native</a> <br>
		      		<!-- <a target="_blank" href="mailto:jay.dee.force@gmail.com?subject=Hello I need help with a Wordpress Project">&middot; Wordpress</a> <br> -->
		      	</span>
		    </div>
			<h2><span id="typed1" style="white-space:pre;"></h2>
			<h2 class="tech-list"><span id="typed2"></h2>
		</div>
		<div class="col-md-6 mt-6">
			<div class="capture">
				<img class="img-fluid mt-2" src="/img/capture.png" alt="">
				<h2 class="capture-button mt-2"><a target="_blank" href="mailto:jay.dee.force@gmail.com?subject=Hello I need a Quote&body=Hello, I was wondering how much it would cost to {Your project or task description here}"" role="button">Get Quote</a></h2>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col mb-2">
			<div class="jumbotron">
				<p>Software development can be hard. If you have the right set up and the best development workflow, your project is guaranteed to become a solid system. However, this is achieved with a combination of expertise, hard work and adoption of proper standards. I have the experience your project needs to become a nice and robust system.</p>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12 my-5 pb-4">
			<h1>Don't scroll here, don't be shy and ping me up for a quote.</h1>
			<p>I believe good communication is the basis of successful projects, don't be shy and ask me for a quote for your project. Even if you don't hire me, I will help you get a better idea for your project.</p>
		</div>
	</div>
	
</div>

@endsection

@push('scripts')
<script src="/js/typed.min.js"></script>
<script>
		
	$('.capture').hide();

	var typed1 = new Typed('#typed1', {
		stringsElement: '#typed-strings1',
		typeSpeed: 12,
		backSpeed: 0,
		backDelay: 500,
		startDelay: 500,
		loop: false,
		onComplete: function (self){
			$('.typed-cursor').addClass('d-none');
			initTyped2();
		}
	});

	function initTyped2(){

		var typed1 = new Typed('#typed2', {
	    stringsElement: '#typed-strings2',
		    typeSpeed: 12,
		    backSpeed: 0,
		    backDelay: 500,
		    startDelay: 500,
		    loop: false,
		    onComplete: function(){
		    	$('.capture').fadeIn(2000);
			}
		});
	}
</script>
@endpush