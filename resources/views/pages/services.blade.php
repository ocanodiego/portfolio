@extends('layout.app')

@section('content')

<style>
  .embed-container { 
    position: relative; 
    padding-bottom: 56.25%; 
    height: 0; 
    overflow: hidden; 
    max-width: 100%;
  } 

  .embed-container iframe, .embed-container object, .embed-container embed { 
    position: absolute;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%; 
  }

  .btn.btn-dark{
      color: #ffffff;
      background-color: #202020;
      border-color: #202020;
  }

  .btn.btn-dark:hover{
      background-color: #ccc;
      border-color: #ccc;
  }
</style>

<div class="container mt-5">
  <div class="row">
    <div class="col-md-12 text-center mb-2">
      <h1>SERVICES</h1>
      <main class="text-left">
        <section class="scaling mt-5">
          <h2>Scaling and Systems Architecture</h2>
          <p>Want to scale to thousands or even millions of users? This is a big challenge I can help you make less stressful, more satisfying. While ensuring we are using reliable, efficient solutions that scale and adapt to your needs and the future.</p>
        </section>
        <section class="team-setup mt-5">
          <h2>Tech Team Set Up</h2>
          <p>Looking to hire your own technology team? I'm here to help, with my extensive experience in the field and a very keen eye for talent, I can help you hire the right people for the right job for your technology project. I can help you screen candidates and craft effective coding assessments and interview questions specific to each role.</p>
        </section>
        <section class="mvp mt-5">
          <h2>MVP Development</h2>
          <p>I can help you build your MVP from scratch and make it a reality. Starting with the mindset and preparation for scaling from the start.</p>
        </section>
        <section class="solutions mt-5">
          <h2>Solutions Research</h2>
          <p>Are you looking for the best engineering solution to a particular problem, with your existing systems? Let me help you undergo careful, extensive research that will ensure your solutions are well planned and implemented, considering your context and existing systems. I will craft beautiful and clear documentation that will help your technology team(s), plan, implement and maintain for the next version of your system(s).</p>
        </section>
        <section class="implementation mt-5">
          <h2>Detailed Implementation and Analysis Documents</h2>
          <p>Whether you need a detailed analysis for a new product or help with a painful issue your team is facing, my experience will prove of immense value. My expertise will save you stress, money, and set clear goals within your development team, leading to happier and more efficient developers.</p>
        </section>
        <section class="tech-stack mt-5">
          <h2>Tech Stack Research and Decision Making</h2>
          <p>If you're unsure which technology to use, let me put them to the test. With my service, you'll receive a full review and small implementation to determine which technology best fits your needs. Choosing the right tools for the right project will save you hundreds, if not thousands, of development hours, directly translating into savings.</p>
        </section>
        <section class="code-audits mt-5">
          <h2>Extensive Code Audits</h2>
          <p>I will dig deep into your code and identify areas of opportunity. Among the following:</p>
          <ul>
            <li>Spot possible security vulnerabilities</li>
            <li>Identify coding and engineering inconsistencies</li>
            <li>Identifying areas of improvement with your testing set up</li>
            <li>Review code quality, from indentation and style to proper alignment to software development principles</li>
          </ul>
        </section>
        <section class="tech-stack mt-5">
          <h2>Want me as a part of your team?</h2>
          <p>Let's discuss and get started helping you succeed in your existing or new software development projects.</p>
        </section>
      </main>
    </div>
  </div>
  <div class="row">

  </div>
  @include('layout.thatsme')
</div>

@endsection
