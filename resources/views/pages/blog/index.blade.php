@extends('layout.app')

@section('content')
<div class="container">
	<div class="row">
	@foreach($partners as $partner)
		<div class="col-12 partner-row mb-4">
			<div class="row justify-content-md-center">
				<div class="col-12 text-center">
					<h2>
						@if($partner->isNone()) 
							Portfolio: 
						@else In collaboration with: 
							<a href="{{$partner->site_url}}" target="_blank">{{$partner->name}}</a>
						@endif
					</h2>
				</div>
				@foreach($partner->projects as $project)
					<div class="col-md-4 portfolio-item mt-4">
						<div class="content">
							<img class="img-fluid" src="{{$project->public_thumbnail}}" alt="">
							<div class="overlay open-modal" data-target="#{{$project->slug}}-modal">
								<a href="#" class="project-title">{{$project->short_name}}</a>
							</div>
						</div>
					</div>
				@endforeach

			</div>
		</div>
	@endforeach
	</div>
	<div class="row mt-5">
		<div class="col-8 col-md-3 mx-auto mb-5">
			<img class="img-fluid me-pic" src="/img/me.jpg" alt="">
		</div>
		<div class="col-md-9 my-auto">
			<h2>That's me!</h2>
			<p class="mb-5">I consider myself to be an interdisciplinary developer with courage to explore many fields, I believe in organization and discipline as the motor of my achievements. I have experience in PHP, Java and Javascript (server side and client side). I'll be pretty straightforward and efficient with wordpress and laravel projects because that's my area of expertise, however I'm not scared with any technology.</p>
		</div>
	</div>
</div>

@endsection
