@extends('layout.app')
@section('content')
<div class="jumbotron m-0">
	<h1 class="display-4">Diego Ponciano</h1>
	<p class="lead">Software Engineer</p>
	<hr class="my-4">
	<p>Senior Full Stack Software Engineer with 7 years of intense and meaninful experience in the industry. Experienced in all stages of the development cycle for complex dynamic web projects, from small to enterprise scale. With a focus for strong, long term relationships with clients.</p>
	<div class="row">
		<div class="col-12 text-center">
			<h2 class="mb-4">Experience (most relevant clients):</h2>
		</div>
		<div class="col-12 col-lg-8 mx-auto">
			<table class="table table-striped">
				<tbody>
					<tr>
						<th style="width: 30%" scope="row">T2 (San Francisco, CA) 2022 - present</th>
						<td>T2 is a public social media platform with a waiting list of around 15K users and with hopes to scale largely. My role at T2 is planning and architechting existing and future solutions to scale and maitain our product services active and reliable. Help develop and maintain features and fix issues. Coordinate team members to best address the system needs and cooperate with engineers to solve and scale.</td>
			    </tr>
					<tr>
						<th style="width: 30%" scope="row">Havona (Australia and Singapore) 2021 - 2022</th>
						<td>Havona is a web and mobile social network with intentions to launch in Australia and the European market. My role here was as a Principal Software Engineer, helping manage, architect and maintain features with small yet very talented team of developers.</td>
			    </tr>
					<tr>
						<th style="width: 30%" scope="row">Mai36 Art Gallery (Zurich, Switzerland) 2020 - present</th>
						<td>My role here is to push new features and maintain existing ones. Also keep their systems updated and upgraded. We have added many new features to the website and many more are coming.</td>
			    </tr>
					<tr>
						<th style="width: 30%" scope="row">Mike's Bikes (SF, USA)<br> 2020 - present</th>
						<td>Built a full stack system for internal use as a contractor. The system provides an easy to use interface for technicians to track their time and materials used. Laravel and Vue.js was used.</td>
			    </tr>
					<tr>
						<th style="width: 30%" scope="row">LinkingArts (New Orleans, USA) 2019</th>
						<td>Provided advise for existing systems improvements and refactoring. Helped align to Laravel standards, helped on most parts of development cycle including QA on various web projects. </td>
			    </tr>
					<tr>
						<th style="width: 30%" scope="row">Modpart (California, USA) 2018</th>
						<td>Full stack developer, analyst and adviser. We developed
						a powerful web application based on Elastic Search, Laravel
						and Vue.js for a niche business opportunity. </td>
			    </tr>
					<tr>
						<th style="width: 30%" scope="row">Synapse Tech (Cambridge, UK) 2018</th>
						<td>Lead developer, technology advisor, quality assurance and front line developer. My main job was to build and maintain features and fix bugs across a very complex system, which we built from scratch. The project has been suspended by the founders.</td>
			    </tr>
					<tr>
						<th style="width: 30%" scope="row">Sporta Gym (Guatemala, Guatemala City) 2017</th>
						<td>Guatemalan Gym chain, they required advice, maitenance and development for their web systems (public and internal).</td>
			    </tr>
					<tr>
						<th style="width: 30%" scope="row">Telus International (Guatemala, Guatemala City) 2016</th>
						<td>Customer Service for a technology company based on San Franscisco, CA. My roles were CSR for 8 months, Trainer Mentor for 8 months and Supervisor for over a year. My responsibilities were, ensuring performance of a team of more than 16 agents and provide timely feedback for service improvement.</td>
			    </tr>
				</tbody>
			</table>
		</div>
	</div>
	<div class="row">
		<div class="col-12 text-center">
			<h2 class="mb-4">Studies:</h2>
		</div>
		<div class="col-12 col-lg-8 mx-auto">
			<table class="table">
				<tbody>
					<tr>
						<th style="width: 40%" scope="row">Systems Engineering (2014 - 2016)</th>
						<td>Mariano Galvez University</td>
			    	</tr>
					<tr>
						<th style="width: 40%" scope="row">High School (2009 - 2013)</th>
						<td>Valles School</td>
			    	</tr>
					
				</tbody>
			</table>
		</div>
<!-- 		<div class="col-12 text-right mt-4">
			<p class="lead">
				<a class="btn btn-gold btn-lg" href="/Diego Ponciano - Software Engineer - Light Resume.pdf" download role="button">Download</a>
			</p>
		</div> -->
	</div>
</div>


@endsection