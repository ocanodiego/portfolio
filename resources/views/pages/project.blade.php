@extends('layout.app')

@section('content')

<style>
	.embed-container { 
		position: relative; 
		padding-bottom: 56.25%; 
		height: 0; 
		overflow: hidden; 
		max-width: 100%;
	} 

	.embed-container iframe, .embed-container object, .embed-container embed { 
		position: absolute;
		top: 0;
		left: 0;
		width: 100%;
		height: 100%; 
	}
</style>

<div class="container mt-5">
	<div class="row">	
		<div class="col-sm-12 text-center ">
			<h1>{{$project->short_name}}</h1>
		</div>
		<div class="col-12 d-block d-sm-none text-center mt-5">
			<p style="white-space: pre-line" class="fs-24">{{$project->description}}</p>
		</div>
		<div class="col-12 col-md-7 photo-show mt-5">
    	@foreach($project->images()->orderByDesc('created_at')->get() as $image)
				<img class="mb-4 gallery-item" src="{{$image->public_url}}" alt="">
			@endforeach
    </div>
		<div class="col-12 col-md-5 text-center mt-2 mt-sm-5">
			<p class="d-none d-sm-block" style="white-space: pre-line">
				{{$project->description}}
				@if($project->url)

				<a target="_blank" href="{{$project->url}}">Visit live project <i class="fas fa-external-link-alt"></i></a>
				@endif

				<a href="{{route('portfolio')}}"><i class="fas fa-arrow-circle-left"></i> See more projects</a>
	    	</p>
	    	<p style="white-space: pre-line" class="d-block d-sm-none fs-24">
	    		@if($project->url)
				<a target="_blank" href="{{$project->url}}">Visit live project <i class="fas fa-external-link-alt"></i></a>
				@endif

				<a href="{{route('portfolio')}}"><i class="fas fa-arrow-circle-left"></i> See more projects</a>
	    	</p>
		</div>
	</div>
	@include('layout.thatsme')
</div>

@endsection

@push('scripts')

	<script>

	</script>

@endpush