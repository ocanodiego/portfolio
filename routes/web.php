<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

Route::get('/', 'PublicPagesController@home');
Route::get('/portfolio', 'PublicPagesController@portfolio')->name('portfolio');
Route::get('/resume', 'PublicPagesController@resume');
Route::get('/services', 'PublicPagesController@services');
Route::get('/modeling', 'PublicPagesController@modeling');
Route::get('/project/{project}', 'ProjectController@show');
Route::get('/project/slug/{project_slug}', 'ProjectController@slug')->name('project.show');
Route::get('/contact', 'PublicPagesController@contact');


Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});
