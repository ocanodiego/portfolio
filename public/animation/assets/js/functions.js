//MAIN

$( document ).ready(function() {
	smoothScroll(1500);
    $("#menuicon").click(moveclouds);
    divideMobile();
    $(dialog);
});

//VARIABLES

var time = Math.random() * (0.8 - 0.5) + 0.5;
var elevate = 1; //Decreases scrollTop value on scroll document function by dividing it

//FUNCTIONS

// All functions run in the document ready function

function smoothScroll (duration) {
	$('a[href^="#"]').on('click', function(event) {

	    var target = $( $(this).attr('href') );

	    if( target.length ) {
	        event.preventDefault();
	        $('html, body').animate({
	            scrollTop: target.offset().top
	        }, duration);
	    }
	});
}

function moveclouds(){
    $('div[class^="nube"]').toggleClass("menu");

    $('.nubeuno').removeAttr("style");
    $('.nubedos').removeAttr("style");
    $('.nubetres').removeAttr("style");
    $('.nubecuatro').removeAttr("style");
    $('.nubecinco').removeAttr("style");

    $('.nubeuno.menu').css({"-webkit-animation":"none","-moz-animation":"none","-o-animation":"none", "animation":"none" });
    $('.nubedos.menu').css({"-webkit-animation":"none","-moz-animation":"none","-o-animation":"none", "animation":"none" });
    $('.nubetres.menu').css({"-webkit-animation":"none","-moz-animation":"none","-o-animation":"none", "animation":"none" });
    $('.nubecuatro.menu').css({"-webkit-animation":"none","-moz-animation":"none","-o-animation":"none", "animation":"none" });
    $('.nubecinco.menu').css({"-webkit-animation":"none","-moz-animation":"none","-o-animation":"none", "animation":"none" });

    $('.thumb-overlay').toggleClass('active');
    $('.nubeuno.menu').css({"-webkit-animation":"bounce 4000ms linear both","-moz-animation":"bounce 4000ms linear both","-o-animation":"bounce 4000ms linear both", "animation":"bounce 4000ms linear both" });
    $('.nubedos.menu').css({"-webkit-animation":"bounce 4000ms linear both","-moz-animation":"bounce 4000ms linear both","-o-animation":"bounce 4000ms linear both", "animation":"bounce 4000ms linear both" });
    $('.nubetres.menu').css({"-webkit-animation":"bounce 4000ms linear both","-moz-animation":"bounce 4000ms linear both","-o-animation":"bounce 4000ms linear both", "animation":"bounce 4000ms linear both" });
    $('.nubecuatro.menu').css({"-webkit-animation":"bounce 4000ms linear both","-moz-animation":"bounce 4000ms linear both","-o-animation":"bounce 4000ms linear both", "animation":"bounce 4000ms linear both" });
    $('.nubecinco.menu').css({"-webkit-animation":"bounce 4000ms linear both","-moz-animation":"bounce 4000ms linear both","-o-animation":"bounce 4000ms linear both", "animation":"bounce 4000ms linear both" });

}

function divideMobile(){

    if( $('.nav').css('display')=='none') {
        elevate = 2;    
        console.log(elevate);
        console.log('true');
}

}

$(document).scroll(function () {

    var y = $(this).scrollTop();

    if (y > 700/elevate) {
            setTimeout(function(){ $('.visualiza').css({visibility: "visible"}).animate({opacity: 1}, 'slow');}, 200);
    }

    if (y > 1000/elevate) {
            setTimeout(function(){$('.tomato').css({visibility: "visible"}).animate({opacity: 1}, 'slow');}, 200);
    }

    if (y > 1400/elevate) {
            setTimeout(function(){$('.helado').css({ visibility: "visible"}).animate({opacity: 1}, 'slow');}, 200);
    }
    if (y > 2100/elevate) {
            setTimeout(function(){$('.gym').css({ visibility: "visible"}).animate({opacity: 1}, 'slow');}, 200);
    }

    if (y > 2500/elevate) {
            setTimeout(function(){$('.industria').css({ visibility: "visible"}).animate({opacity: 1}, 'slow');}, 200);
    }

    if (y > 800/elevate) {
        $('#original').fadeIn();
    }else{
        $('#original').hide();
    }

    if (y > 1000/elevate) {
        $('#original-6').fadeIn();
    }else{
        $('#original-6').hide();
    }
    if (y > 1200/elevate) {
        $('#original-2').fadeIn();
    }else{
        $('#original-2').hide();
    }
    if (y > 1400/elevate) {
        $('#original-7').fadeIn();
    }else{
        $('#original-7').hide();
    }
    if (y > 1600/elevate) {
        $('#original-3').fadeIn();
    }else{
        $('#original-3').hide();
    }
    if (y > 1800/elevate) {
        $('#original-8').fadeIn();
    }else{
        $('#original-8').hide();
    }
    if (y > 2000/elevate) {
        $('#original-4').fadeIn();
    }else{
        $('#original-4').hide();
    }
    if (y > 2200/elevate) {
        $('#original-9').fadeIn();
    }else{
        $('#original-9').hide();
    }
    if (y > 2400/elevate) {
        $('#original-5').fadeIn();
    }else{
        $('#original-5').hide();
    }
    if (y > 2600/elevate) {
        $('#original-10').fadeIn();
    }else{
        $('#original-10').hide();
    }

/////Plants/////

    if (y > 800/elevate) {    

    	setTimeout(function(){$("path[group=plant-1]").fadeIn();}, 3000*time);
    	setTimeout(function(){$("path[group=plant-2]").fadeIn();}, 4000*time);
    	setTimeout(function(){$("path[group=plant-3]").fadeIn();}, 5000*time);
    	setTimeout(function(){$("path[group=plant-4]").fadeIn();}, 2000*time);
    	setTimeout(function(){$("path[group=plant-5]").fadeIn();}, 1000*time);
    	setTimeout(function(){$("path[group=plant-6]").fadeIn();}, 3500*time);
    	setTimeout(function(){$("path[group=plant-7]").fadeIn();}, 5000*time);
    	setTimeout(function(){$("path[group=plant-8]").fadeIn();}, 4500*time);
    	setTimeout(function(){$("path[group=plant-9]").fadeIn();}, 5500*time);
    	setTimeout(function(){$("path[group=plant-10]").fadeIn();}, 4800*time);
    	setTimeout(function(){$("path[group=plant-11]").fadeIn();}, 4600*time);
    	setTimeout(function(){$("path[group=plant-12]").fadeIn();}, 3200*time);
    	setTimeout(function(){$("path[group=plant-13]").fadeIn();}, 3400*time);
    	setTimeout(function(){$("path[group=plant-14]").fadeIn();}, 2800*time);
    	setTimeout(function(){$("path[group=plant-15]").fadeIn();}, 1700*time);
    	setTimeout(function(){$("path[group=plant-16]").fadeIn();}, 2700*time);
    	setTimeout(function(){$("path[group=plant-17]").fadeIn();}, 3000*time);
    	setTimeout(function(){$("path[group=plant-18]").fadeIn();}, 4000*time);
    	setTimeout(function(){$("path[group=plant-19]").fadeIn();}, 5000*time);
    	setTimeout(function(){$("path[group=plant-20]").fadeIn();}, 5500*time);
    	setTimeout(function(){$("path[group=plant-21]").fadeIn();}, 2200*time);
    	setTimeout(function(){$("path[group=plant-22]").fadeIn();}, 4100*time);
    	setTimeout(function(){$("path[group=plant-23]").fadeIn();}, 2800*time);
    	setTimeout(function(){$("path[group=plant-24]").fadeIn();}, 2300*time);
    }

});

function dialog() {

  // Declare variables
  var dialogBox = $('.dialog'),
      dialogTrigger = $('.dialog__trigger'),
      dialogClose = $('.dialog__close'),
      dialogTitle = $('.dialog__title'),
      dialogContent = $('.dialog__content')
      dialogContentJerk = $('.dialog__content .jerk'),
      dialogAction = $('.dialog__action'),

      dialogPrimero = $('.primero'),
      dialogSegundo = $('.segundo'),
      dialogTercero = $('.tercero'),
      dialogCuarto = $('.cuarto'),
      dialogQuinto = $('.quinto'),
      dialogSexto = $('.sexto'),
      dialogSeptimo = $('.septimo'),
      dialogOctavo = $('.octavo'),

      dialogMail = $('.mail'),
      dialogPhone = $('.phone'),

      dialogNubeuno = $('.nubeuno'),
      dialogNubedos = $('.nubedos'),
      dialogNubetres = $('.nubetres'),
      dialogNubecuatro = $('.nubecuatro'),
      dialogNubecinco = $('.nubecinco');

dialogNubeuno.on('click', function(){

        $('.dialog__content .jerk').remove();
        dialogTitle.text("Nuestra organización");
        dialogContent.append("<div class='jerk'> <p>Somos un conjunto de entusiastas, freelancers, dise&ntilde;adores, programadores y analistas independientes, con amplia experiencia en su &amp;aacute;rea respectiva.</p> </div>");
});

dialogNubedos.on('click', function(){

        $('.dialog__content .jerk').remove();
        dialogTitle.text("Productos");
        dialogContent.append("<div class='jerk'> <p>Ofrecemos productos de toda clase, desde simples p&aacute;ginas web estaticas y profesionales, hasta complejos sistemas para uso industrial.</br></br>- Simples: P&aacute;ginas web estaticas, sesiones de fotograf&iacute;a profesional, creaci&oacute;n de videos promocionales, etc.</br>- Complejos: E-commerce websites, desarrollo de sistemas para monitoreo remoto, creaci&oacute;n de p&aacute;ginas web con productos personalizables, etc. </br>- Industriales: Sistemas con alta complejidad de funcionamiento como gesti&oacute;n de PLCs o Arduinos remota o por sistema nativo, software corporativo, software para administrar lineas de producci&oacute;n, etc.</p> </div>");
});

dialogNubetres.on('click', function(){

        $('.dialog__content .jerk').remove();
        dialogTitle.text("Nosotros");
        dialogContent.append("<div class='jerk'> <p>Nuestro fundador y programador principal: <a href='https://poncianodiego.github.io/portfolio/'> Juan Diego Ponciano</a></br> L&iacute;der creativo de video <a href='https://www.youtube.com/channel/UCQboRJw9B609X0fURpr2htg'> Jorge Ardón </a></br> Asesor&iacute;a profesional y diseño: Mauricio Gonzalez</p> </div>");
});

dialogNubecuatro.on('click', function(){

        $('.dialog__content .jerk').remove();
        dialogTitle.text("Educación");
        dialogContent.append("<div class='jerk'> <p>Contamos con tutorias de diseño web y programaci&oacute;n profesional a domicilio.</p><h3> Ll&aacute;manos para m&aacute;s informaci&oacute;n: 6640 1630 <h3></div>");
});

dialogNubecinco.on('click', function(){

        $('.dialog__content .jerk').remove();
        dialogTitle.text("Contactar");
        dialogContent.append("<div class='jerk'> <h3>Mail: wdevforyou@gmail.com </br> Tel&eacute;fono: 6640 1630</h3> </div>");

});

dialogMail.on('click', function(){

        $('.dialog__content .jerk').remove();
        dialogTitle.text("Ser&aacute; un gusto responderte en un tiempo record!");
        dialogContent.css("font-size", "5vw");
        dialogContent.append("<div class='jerk'> <p>wdevforyou@gmail.com</p> </div>");
});

dialogPhone.on('click', function(){

        $('.dialog__content .jerk').remove();
        dialogTitle.text("Nuestras l&iacute;neas estan abiertas!");
        dialogContent.css("font-size", "5vw");
        dialogContent.append("<div class='jerk'> <p>+(502) 6640 1630</p> </div>");
});

//Replace content and change font height and width accordingly

dialogPrimero.on('click', function(){

        $('.dialog__content .jerk').remove();
        dialogTitle.text("Imágenes Vectoriales");
        dialogContent.append("<div class='jerk'><p>Una imagen vectorial es una im&aacute;gen generada a partir de vectores matem&aacute;ticos o coordenadas. &lt;/p&gt;&lt;p&gt;Una imagen vectorial tiene la capacidad de poner en uso toda la definici&oacute;n disponible de cualquier pantalla electr&oacute;nica. Por estas razones las im&aacute;genes vectoriales son una forma profesional de presentar contenido. Al acercar una imagen vectorial digamos un 64,000% esta nunca se degradar&aacute;, ya que es un vector matem&aacute;tico que se adapta m&aacute;s que inmediatamente. En un detalle un poco m&aacute;s t&eacute;cnico, una imagen vectorial ofrece la posibilidad de modificar directamente su estilo de cualquier forma desde un documento CSS (Cascading Style Sheet). </p><p>Imagine esta tecnolog&iacute;a trabajando para usted e imagine las posibilidades. Utilizar im&aacute;genes vectoriales en el dise&ntilde;o web de su empresa definitivamente le permitir&aacute; dar a conocer sus servicios de forma profesional y siempre seguro que el cliente ver&aacute; su contenido con la mejor calidad posible. </p></div>");
});

dialogSegundo.on('click', function(){

        $('.dialog__content .jerk').remove();
        dialogTitle.text("Diseño Limpio");
        dialogContent.append("<div class='jerk'> <p> Se sabe cuando algo se encuentra limpio y presentable. La imagen hoy en d&iacute;a (y como siempre) ha representado estatus, solidez, seguridad y sobre todo profesionalismo en lo que se hace. Un dise&ntilde;o limpio y acorde es definitivamente indispensable en toda empresa seria. Como parte de la estrategia de mercado de cualquier empresa o proyecto se debe tener en cuenta un dise&ntilde;o limpio. Esto significa, un dise&ntilde;o libre de elementos innecesarios, libre de contaminaci&oacute;n visual y asi mismo, fluido y congruente al contenido. </p> </div>");
});

dialogTercero.on('click', function(){

        $('.dialog__content .jerk').remove();
        dialogTitle.text("Código Limpio");
        dialogContent.append("<div class='jerk'> <p> Podr&iacute;amos toparnos con un sistema que presenta un excelente dise&ntilde;o, muy limpio por naturaleza y muy bien pensado en cuanto a la posici&oacute;n de los elementos. Llega el momento de hacer ediciones y la persona a cargo no es necesariamente la persona o grupo de personas que desarrollaron el sistema inicialmente. Si no se tiene un c&oacute;digo presentable y bien estructurado, el trabajo del nuevo equipo se ver&aacute; obstaculizado enormemente al momento de hacer cambios en el mismo, esto, aunque el mencionado equipo de trabajo tenga mucha experiencia y conocimiento. Un c&oacute;digo limpio debe ser tambi&eacute;n un c&oacute;digo eficiente, en el cual, cada implementaci&oacute;n se hace con la menor cantidad posible de l&iacute;neas de c&oacute;digo y estas cumplen con la funci&oacute;n que se pretende asignarles. Adem&aacute;s de esto, un c&oacute;digo eficiente y limpio toma en consideraci&oacute;n posibles cambios en el futuro en la medida de lo posible. Cada secci&oacute;n se encuentra muy bien identificada.</p></div>");
});

dialogCuarto.on('click', function(){

        $('.dialog__content .jerk').remove();
        dialogTitle.text("Desarrollo Profesional");
        if( $('.nav').css('display')=='none') {
            dialogContent.css("font-size", "2.4vw");
            dialogContent.css("line-height", "4vw");
        }else{
            dialogContent.css("font-size", "1.2vw");
            dialogContent.css("line-height", "2vw");
        }
        dialogContent.append("<div class='jerk'> <p>&iquest;C&oacute;mo asegurar la mejor calidad posible en el menor tiempo posible? Es una pregunta que todo emprendedor serio se formula constantemente para alcanzar la calidad de servicio deseada. </br></br>Aunque la respuesta es sencilla, no todos los emprendedores logran visualizar y poner en marcha sistemas que garanticen lo mejor.</br></br>- Se utiliza el talento. Cuando se tiene talento se debe explotar al m&aacute;ximo sin ning&uacute;n miedo. </br>- Se repite. Las cosas se repiten una y otra vez y se reformulan los problemas de distintas formas. </br>- Se repiensa todo desde cero. </br>- Se es un espectador. Al desarrollar un producto, constantemente se debe ver todo desde el punto de vista de un espectador, alguien quien no sabe nada del producto o servicio y tambi&eacute;n se observa desde el punto de vista de un consumidor definitivo del producto. </br>- Se pide retroalimentaci&oacute;n. De todos. De todo. Y esta se escucha con atenci&oacute;n y se implementa toda en la medida de lo posible. Sin quebrantar el dise&ntilde;o inicial ni la esencia del producto. </br>- Se es creativo. Un verdadero profesional toma riesgos porque los puede manejar. Punto. </br></br>Cinco pasos sencillos de escribir, extremadamente dificil de poner en pr&aacute;ctica. Pero lo siguiente es seguro, desde el m&aacute;s m&iacute;nimo esfuerzo hasta el mayor sacrificio tendr&aacute;n una incomparable recompensa y esto lo sabe todo profesional exitoso y dedicado.</p> </div>");

});

dialogQuinto.on('click', function(){

        $('.dialog__content .jerk').remove();
        dialogTitle.text("Agile PM");
        if( $('.nav').css('display')=='none') {
            dialogContent.css("font-size", "2.2vw");
            dialogContent.css("line-height", "3vw");
        }else{
            dialogContent.css("font-size", "1.1vw");
            dialogContent.css("line-height", "1.5vw");
        }
        dialogContent.append("<div class='jerk'> <p>Comencemos por entender qu&eacute; es el PMI o project management institute. El PMI es una organizaci&oacute;n estadounidense sin fines de lucro que asocia a profesionales relacionados con la Gesti&oacute;n de Proyectos.</br></br> Desde principios de 2011, es la m&aacute;s grande del mundo en su rubro, dado que se encuentra integrada por cerca de 500 000 miembros en casi 100 pa&iacute;ses. La oficina central se encuentra en la localidad de Newtown Square, en la periferia de la ciudad de Filadelfia, en Pensilvania (Estados Unidos). Sus principales objetivos son:</br></br>- Formular est&aacute;ndares profesionales en Gesti&oacute;n de Programas. </br>- Generar conocimiento a trav&eacute;s de la investigaci&oacute;n. </br>- Promover la Gesti&oacute;n de Proyectos como profesi&oacute;n a trav&eacute;s de sus programas de certificaci&oacute;n. </br></br>Ya que sabemos un poco acerca de qu&eacute; es el PMI podemos continuar. Agile project management es un estilo de manejo de proyectos y su enfoque es el de generar un producto o deliverable bajo los mejores estandares y la mejor calidad, en el menor tiempo posible. Esto se logra gracias a la rigurosa aplicaci&oacute;n de principios que marcan este estilo de colaboraci&oacute;n en manejo de proyectos. Se debe contar con un equipo de trabajo altamente calificado y con una excelente calidad humana. Los valores juegan un papel muy importante en este estilo y sin ellos la realizaci&oacute;n de un proyecto ser&iacute;a totalmente imposible. En orden jer&aacute;rquico, luego de la calidad humana se podr&iacute;a mencionar el principio de iteraci&oacute;n del m&eacute;todo. En el cual se pretende desarrollar productos de forma escalonada e iterativa. Al decir de forma escalonada, nos referimos al lanzamiento consecutivo del producto, presentando este, mejores caracter&iacute;sticas en cada uno de los nuevos lanzamientos o iteraciones. Esta metodolog&iacute;a se aplica a proyectos que exigen un alto nivel de creatividad y presentan un alto nivel de riesgo e incertidumbre (cuando no se sabe con exactitud cu&aacute;l ser&aacute; el producto final). Es decir, es una metodolog&iacute;a perfecta para el desarrollo de software de todo tipo. Siempre es un excelente d&iacute;a para aprender acerca de esta maravillosa metodolog&iacute;a desarrollada por los mejores del mundo en manejo de proyectos de toda naturaleza.</p> </div>");

});

dialogSexto.on('click', function(){

        $('.dialog__content .jerk').remove();
        dialogTitle.text("Ingredientes Ligeros Vs Funcionalidad");
        dialogContent.append("<div class='jerk'> <p>&iquest;Colocar un PNG, JPG o SVG? &iquest;Utilizar una librer&iacute;a o no? &iquest;Sacrificar calidad por rapidez en un video? Estas son preguntas comunes que solo un profesional sabe responder con exactitud siempre que el cliente tenga una visi&oacute;n clara del producto final esperado. Es todo un reto tomar estas decisiones si no se tienen los detalles t&eacute;cnicos que hacen mejor una opci&oacute;n que la otra. Ya que el mundo de la tecnolog&iacute;a es inmenso y muy cambiante, es a&uacute;n m&aacute;s retante. Las empresas de tecnolog&iacute;a deben adaptarse extremadamente r&aacute;pido y aprender nuevas tecnolog&iacute;as todos los d&iacute;as para mantenerse a flote. Idealmente una empresa tecnol&oacute;gica marca el paso con la introducci&oacute;n de m&eacute;todos creados por la misma, o como un m&iacute;nimo sabe aplicar las mejores tecnolog&iacute;as y puede responder por ejemplo, porque utiliza X base de datos en lugar de Y. </p> </div>");

});

dialogSeptimo.on('click', function(){

        $('.dialog__content .jerk').remove();
        dialogTitle.text("Casos De Estudio");
        if( $('.nav').css('display')=='none') {
             dialogContent.css("line-height", "7vw");
        }
        dialogContent.append("<div class='jerk'><a href='http://yoli.gt/'>Yoli</a></br><a href='http://waaark.com/'> Waaark</a></br><a href=''> WebDev</a></br></div>");

});

dialogOctavo.on('click', function(){

        $('.dialog__content .jerk').remove();
        dialogTitle.text("Un Mundo De Posibilidades");
        if( $('.nav').css('display')=='none') {
            dialogContent.css("font-size", "2.4vw");
            dialogContent.css("line-height", "3.2vw");
        }else{
            dialogContent.css("font-size", "1.2vw");
            dialogContent.css("line-height", "1.8vw");
        }
        dialogContent.append("<div class='jerk'> <p>El momento hist&oacute;rico exige una alta atenci&oacute;n al desarrollo y utilizaci&oacute;n de las mejores tecnolog&iacute;as. Una vez el ser humano descubri&oacute; las herramientas, comenz&oacute; a dominar su entorno. En el presente se tienen al alcance de la mano miles de millones de distintos tipos de herramientas y entre las m&aacute;s poderosas est&aacute;n las que nos permite comunicar e inter conectarnos con el resto del mundo. Las posibilidades son incre&iacute;bles y lo mejor de todo es que hoy en d&iacute;a son realizables, s&oacute;lo se requiere de un poco de fe, creatividad, arduo trabajo y un toque de locura para so&ntilde;ar alto y hacer nuestros sue&ntilde;os m&aacute;s c&aacute;lidos y futuristas, una realidad. Te invito a ver y conocer acerca de los proyectos m&aacute;s ambiciosos del momento:</br></br><a href='https://www.tesla.com/'> Tesla motors</a>: Primera compa&ntilde;ia sustentable de carros el&eacute;ctricos en Estados Unidos</br><a href='https://info.internet.org/en/'> Internet.org<a>: Iniciativa de Mark Zuckerberg para proveer de internet de forma gratuita a comunidades sin acceso a internet.</br><a href='http://www.spacex.com/'> SpaceX<a>: Desarrolladores de tecnolog&iacute;a espacial significativa.</br><a href='http://www.solarcity.com/'> SolarCity</a>: Primera empresa realmente sustentable de energ&iacute;a solar.</br> <a href='http://chanzuckerberg.com/'>Chan Zuckerberg Initiative</a>: Iniciativa fundada con alrededor de 38.1 billones de dolares para impulsar el potencial humano en todo el mundo. </br><a href='https://www.oculus.com/'> Oculus</a>: Realidad Virtual al siguiente nivel.</br><a href='https://www.indiegogo.com/projects/father-io-massive-multiplayer-laser-tag-app#/'>Father.io</a>: Laser Tag Virtual.</br><a href='https://en.wikipedia.org/wiki/List_of_megaprojects'> List of megaprojects</a>: Lista de megaproyectos (no necesariamente impulsan el desarrollo humano como todos los anteriores)</br><a href='https://www.duolingo.com/'>Duolingo</a>: Aprende un idioma totalmente gratis. Fundado por Luis Vohn Ann.</br></br> ¿Qué esperas para ser parte de este cambio GLOBAL?</p> </div>");

});

  // Open the dialog
  dialogTrigger.on('click', function(e) {
    dialogBox.toggleClass('dialog--active');
    e.stopPropagation();
  });

  // Close the dialog - click close button
  dialogClose.on('click', function() {
    dialogBox.removeClass('dialog--active');
    if( $('.nav').css('display')=='none') {
        console.log("Dont change it!")
        dialogContent.css("font-size", "4vw");
        dialogContent.css("line-height", "4vw");
        }else{
        dialogContent.css("font-size", "1.5vw");
        dialogContent.css("line-height", "1.5vw");
        }
    });

  // Close the dialog - press escape key // key#27
  $(document).keyup(function(e) {
    if (e.keyCode === 27) {
      dialogBox.removeClass('dialog--active');
       if( $('.nav').css('display')=='none') {
        console.log("Dont change it!")
        dialogContent.css("font-size", "4vw");
        dialogContent.css("line-height", "4vw");
        }else{
        dialogContent.css("font-size", "1.5vw");
        dialogContent.css("line-height", "1.5vw");
        }
    }
  });

  // Close the dialog - click outside
  $(document).on('click', function(e) {
    if ($(e.target).is(dialogBox) === false &&
        $(e.target).is(dialogTitle) === false &&
        $(e.target).is(dialogContent) === false &&
        $(e.target).is(dialogAction) === false) {
      dialogBox.removeClass('dialog--active');  
       if( $('.nav').css('display')=='none') {
        console.log("Dont change it!")
        dialogContent.css("font-size", "4vw");
        dialogContent.css("line-height", "4vw");
        }else{
        dialogContent.css("font-size", "1.5vw");
        dialogContent.css("line-height", "1.5vw");
        }
    }
  });

}
