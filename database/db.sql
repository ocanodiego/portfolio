-- phpMyAdmin SQL Dump
-- version 4.9.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Nov 19, 2020 at 08:17 PM
-- Server version: 5.6.41-84.1
-- PHP Version: 7.3.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ocanodie_poncianodiego`
--

-- --------------------------------------------------------

--
-- Table structure for table `data_rows`
--

CREATE TABLE `data_rows` (
  `id` int(10) UNSIGNED NOT NULL,
  `data_type_id` int(10) UNSIGNED NOT NULL,
  `field` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `required` tinyint(1) NOT NULL DEFAULT '0',
  `browse` tinyint(1) NOT NULL DEFAULT '1',
  `read` tinyint(1) NOT NULL DEFAULT '1',
  `edit` tinyint(1) NOT NULL DEFAULT '1',
  `add` tinyint(1) NOT NULL DEFAULT '1',
  `delete` tinyint(1) NOT NULL DEFAULT '1',
  `details` text COLLATE utf8mb4_unicode_ci,
  `order` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `data_rows`
--

INSERT INTO `data_rows` (`id`, `data_type_id`, `field`, `type`, `display_name`, `required`, `browse`, `read`, `edit`, `add`, `delete`, `details`, `order`) VALUES
(1, 1, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, '', 1),
(2, 1, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, '', 2),
(3, 1, 'email', 'text', 'Email', 1, 1, 1, 1, 1, 1, '', 3),
(4, 1, 'password', 'password', 'Password', 1, 0, 0, 1, 1, 0, '', 4),
(5, 1, 'remember_token', 'text', 'Remember Token', 0, 0, 0, 0, 0, 0, '', 5),
(6, 1, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 0, 0, 0, '', 6),
(7, 1, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '', 7),
(8, 1, 'avatar', 'image', 'Avatar', 0, 1, 1, 1, 1, 1, '', 8),
(9, 1, 'user_belongsto_role_relationship', 'relationship', 'Role', 0, 1, 1, 1, 1, 0, '{\"model\":\"TCG\\\\Voyager\\\\Models\\\\Role\",\"table\":\"roles\",\"type\":\"belongsTo\",\"column\":\"role_id\",\"key\":\"id\",\"label\":\"display_name\",\"pivot_table\":\"roles\",\"pivot\":\"0\"}', 10),
(10, 1, 'user_belongstomany_role_relationship', 'relationship', 'Roles', 0, 1, 1, 1, 1, 0, '{\"model\":\"TCG\\\\Voyager\\\\Models\\\\Role\",\"table\":\"roles\",\"type\":\"belongsToMany\",\"column\":\"id\",\"key\":\"id\",\"label\":\"display_name\",\"pivot_table\":\"user_roles\",\"pivot\":\"1\",\"taggable\":\"0\"}', 11),
(11, 1, 'locale', 'text', 'Locale', 0, 1, 1, 1, 1, 0, '', 12),
(12, 1, 'settings', 'hidden', 'Settings', 0, 0, 0, 0, 0, 0, '', 12),
(13, 2, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, '', 1),
(14, 2, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, '', 2),
(15, 2, 'created_at', 'timestamp', 'Created At', 0, 0, 0, 0, 0, 0, '', 3),
(16, 2, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '', 4),
(17, 3, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, '', 1),
(18, 3, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, '', 2),
(19, 3, 'created_at', 'timestamp', 'Created At', 0, 0, 0, 0, 0, 0, '', 3),
(20, 3, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '', 4),
(21, 3, 'display_name', 'text', 'Display Name', 1, 1, 1, 1, 1, 1, '', 5),
(22, 1, 'role_id', 'text', 'Role', 1, 1, 1, 1, 1, 1, '', 9),
(23, 4, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, NULL, 1),
(24, 4, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, NULL, 2),
(25, 4, 'description', 'text', 'Description', 1, 1, 1, 1, 1, 1, NULL, 3),
(26, 4, 'site_url', 'text', 'Site Url', 1, 1, 1, 1, 1, 1, NULL, 4),
(27, 4, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, NULL, 5),
(28, 4, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 6),
(29, 4, 'order', 'text', 'Order', 0, 1, 1, 1, 1, 1, NULL, 7),
(30, 5, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, NULL, 1),
(31, 5, 'partner_id', 'text', 'Partner Id', 0, 1, 1, 1, 1, 0, NULL, 2),
(32, 5, 'name', 'text', 'Name', 0, 1, 1, 1, 1, 1, NULL, 3),
(33, 5, 'short_name', 'text', 'Short Name', 0, 1, 1, 1, 1, 1, NULL, 4),
(34, 5, 'slug', 'text', 'Slug', 0, 1, 1, 1, 1, 1, NULL, 5),
(35, 5, 'description', 'text_area', 'Description', 0, 0, 1, 1, 1, 1, NULL, 6),
(36, 5, 'url', 'text', 'Url', 0, 0, 1, 1, 1, 1, NULL, 7),
(37, 5, 'thumbnail', 'image', 'Thumbnail', 0, 1, 1, 1, 1, 1, NULL, 8),
(38, 5, 'video_url', 'text', 'Video Url', 0, 0, 1, 1, 1, 1, NULL, 9),
(39, 5, 'created_at', 'timestamp', 'Created At', 0, 0, 1, 1, 0, 1, NULL, 10),
(40, 5, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 11),
(41, 5, 'project_hasone_partner_relationship', 'relationship', 'partners', 0, 0, 1, 1, 1, 1, '{\"model\":\"App\\\\Partner\",\"table\":\"partners\",\"type\":\"belongsTo\",\"column\":\"partner_id\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"data_rows\",\"pivot\":\"0\",\"taggable\":\"0\"}', 12),
(43, 4, 'partner_hasmany_project_relationship', 'relationship', 'projects', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\Project\",\"table\":\"projects\",\"type\":\"hasMany\",\"column\":\"partner_id\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"data_rows\",\"pivot\":\"0\",\"taggable\":\"0\"}', 8),
(44, 6, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, NULL, 1),
(45, 6, 'url', 'image', 'Url', 1, 1, 1, 1, 1, 1, NULL, 2),
(46, 6, 'relation_id', 'text', 'Relation Id', 1, 0, 1, 1, 1, 1, NULL, 3),
(47, 6, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, NULL, 4),
(48, 6, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 5),
(49, 6, 'image_belongsto_project_relationship', 'relationship', 'projects', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\Project\",\"table\":\"projects\",\"type\":\"belongsTo\",\"column\":\"relation_id\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"data_rows\",\"pivot\":\"0\",\"taggable\":null}', 6),
(50, 5, 'project_hasmany_image_relationship', 'relationship', 'images', 0, 0, 1, 1, 1, 1, '{\"model\":\"App\\\\Image\",\"table\":\"images\",\"type\":\"hasMany\",\"column\":\"relation_id\",\"key\":\"id\",\"label\":\"url\",\"pivot_table\":\"data_rows\",\"pivot\":\"0\",\"taggable\":\"0\"}', 13),
(51, 7, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(52, 7, 'name', 'text', 'Name', 0, 1, 1, 1, 1, 1, '{}', 2),
(53, 7, 'short_name', 'text', 'Short Name', 0, 1, 1, 1, 1, 1, '{}', 3),
(54, 7, 'slug', 'text', 'Slug', 0, 1, 1, 1, 1, 1, '{}', 4),
(55, 7, 'description', 'text', 'Description', 0, 1, 1, 1, 1, 1, '{}', 5),
(56, 7, 'url', 'image', 'Url', 0, 1, 1, 1, 1, 1, '{}', 6),
(57, 7, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 7),
(58, 7, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 8);

-- --------------------------------------------------------

--
-- Table structure for table `data_types`
--

CREATE TABLE `data_types` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name_singular` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name_plural` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `icon` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `model_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `policy_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `controller` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `generate_permissions` tinyint(1) NOT NULL DEFAULT '0',
  `server_side` tinyint(4) NOT NULL DEFAULT '0',
  `details` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `data_types`
--

INSERT INTO `data_types` (`id`, `name`, `slug`, `display_name_singular`, `display_name_plural`, `icon`, `model_name`, `policy_name`, `controller`, `description`, `generate_permissions`, `server_side`, `details`, `created_at`, `updated_at`) VALUES
(1, 'users', 'users', 'User', 'Users', 'voyager-person', 'TCG\\Voyager\\Models\\User', 'TCG\\Voyager\\Policies\\UserPolicy', '', '', 1, 0, NULL, '2018-08-26 00:53:57', '2018-08-26 00:53:57'),
(2, 'menus', 'menus', 'Menu', 'Menus', 'voyager-list', 'TCG\\Voyager\\Models\\Menu', NULL, '', '', 1, 0, NULL, '2018-08-26 00:53:57', '2018-08-26 00:53:57'),
(3, 'roles', 'roles', 'Role', 'Roles', 'voyager-lock', 'TCG\\Voyager\\Models\\Role', NULL, '', '', 1, 0, NULL, '2018-08-26 00:53:57', '2018-08-26 00:53:57'),
(4, 'partners', 'partners', 'Partner', 'Partners', NULL, 'App\\Partner', NULL, NULL, NULL, 1, 0, '{\"order_column\":\"created_at\",\"order_display_column\":\"created_at\"}', '2018-08-27 00:39:04', '2018-08-27 00:39:04'),
(5, 'projects', 'projects', 'Project', 'Projects', NULL, 'App\\Project', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null}', '2018-09-06 01:29:59', '2018-09-06 01:29:59'),
(6, 'images', 'images', 'Image', 'Images', NULL, 'App\\Image', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null}', '2018-09-07 23:47:19', '2018-09-07 23:47:19'),
(7, 'photos', 'photos', 'Photo', 'Photos', NULL, 'App\\Photo', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null}', '2020-02-15 01:02:20', '2020-02-15 01:02:20');

-- --------------------------------------------------------

--
-- Table structure for table `images`
--

CREATE TABLE `images` (
  `id` int(10) UNSIGNED NOT NULL,
  `url` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `relation_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `images`
--

INSERT INTO `images` (`id`, `url`, `relation_id`, `created_at`, `updated_at`) VALUES
(1, 'images/September2018/pDxgAoqyHQRbcme6mXMc.png', '3', '2018-09-07 23:59:48', '2018-09-07 23:59:48'),
(3, 'images/September2018/kZWknkFqFEGseGrSF98Z.png', '3', '2018-09-08 00:09:51', '2018-09-08 00:09:51'),
(4, 'images/September2018/7HHayu4EDXWwwIoMBPbk.png', '3', '2018-09-08 00:10:22', '2018-09-08 00:10:22'),
(5, 'images/September2018/57XmMNr40D8WvE3iDNSO.png', '3', '2018-09-08 00:10:34', '2018-09-08 00:10:34'),
(7, 'images/September2018/NWsgiPEVBF0QtfOZczRz.png', '3', '2018-09-08 00:11:37', '2018-09-08 00:11:37'),
(8, 'images/September2018/Hn1P6EUfkRa2HyRusalE.png', '3', '2018-09-08 00:11:51', '2018-09-08 00:11:51'),
(9, 'images/September2018/k1R7ZFqli8QPErSeP0lX.png', '3', '2018-09-08 00:12:19', '2018-09-08 00:12:19'),
(10, 'images/September2018/c02A5PZszS0c1t68xExd.png', '1', '2018-09-08 00:23:14', '2018-09-08 00:23:14'),
(11, 'images/September2018/vCaAJwavi28noOknQY3Z.png', '1', '2018-09-08 00:23:25', '2018-09-08 00:23:25'),
(12, 'images/September2018/kbCX8IRqLgfhNP4jwcm9.png', '1', '2018-09-08 00:23:36', '2018-09-08 00:23:36'),
(13, 'images/September2018/mWzh1MB9rH4snB5EmcGO.png', '1', '2018-09-08 00:23:51', '2018-09-08 00:23:51'),
(14, 'images/September2018/iPdRFrN56Sr4WWCkgaxO.png', '4', '2018-09-08 00:27:50', '2018-09-08 00:27:50'),
(15, 'images/September2018/EKs5cRmMzpPnLljzOTuI.png', '4', '2018-09-08 00:28:01', '2018-09-08 00:28:01'),
(16, 'images/September2018/5Zk2WGa8t7OWrH06W3Ib.png', '5', '2018-09-08 00:33:52', '2018-09-08 00:33:52'),
(17, 'images/September2018/kRPoPrXcBMUFDMOXCW49.png', '5', '2018-09-08 00:34:09', '2018-09-08 00:34:09'),
(18, 'images/September2018/nCjM2R4ADBUyhNB6TfiA.png', '5', '2018-09-08 00:34:33', '2018-09-08 00:34:33'),
(20, 'images/September2018/IdvNY6jH1Lgq2qX4Qssv.png', '5', '2018-09-08 00:35:15', '2018-09-08 00:35:15'),
(21, 'images/September2018/aiKAPbUMJNYth4i4BBEG.png', '5', '2018-09-08 00:35:37', '2018-09-08 00:35:37'),
(22, 'images/September2018/Orp7eMyrueEWp2xlg0mU.png', '6', '2018-09-08 00:51:01', '2018-09-08 00:51:01'),
(23, 'images/September2018/14r9mtyPzSVK0ATwsGnZ.png', '6', '2018-09-08 00:51:18', '2018-09-08 00:51:18'),
(24, 'images/September2018/aEuKwZfVTuM8Ry8OM2Wz.png', '6', '2018-09-08 00:51:41', '2018-09-08 00:51:41'),
(25, 'images/September2018/A2Qcd42TcXJPm933NlMr.png', '6', '2018-09-08 00:51:59', '2018-09-08 00:51:59'),
(26, 'images/September2018/KCDE2OPR90XqQdfzijwP.png', '7', '2018-09-08 01:24:22', '2018-09-08 01:24:22'),
(27, 'images/September2018/fWiTBlKcVXnUaHsuHTjr.png', '7', '2018-09-08 01:28:58', '2018-09-08 01:28:58'),
(28, 'images/September2018/XXbo9s7T3tSobm76jEBU.png', '7', '2018-09-08 01:29:12', '2018-09-08 01:29:12'),
(30, 'images/September2018/NB5D6wbHUvNWTK5zhxgs.png', '7', '2018-09-08 01:29:35', '2018-09-08 01:29:35'),
(31, 'images/September2018/jiFZ8OYdtwWSZCMQJSBE.png', '8', '2018-09-08 01:36:40', '2018-09-08 01:36:40'),
(32, 'images/September2018/GU78m6saMozriRlGc0aT.png', '8', '2018-09-08 01:37:03', '2018-09-08 01:37:03'),
(33, 'images/September2018/YrE983UywmcH1yuyTyay.png', '8', '2018-09-08 01:37:33', '2018-09-08 01:37:33'),
(34, 'images/September2018/yFdLuyAPWzvLRxuBNkml.png', '8', '2018-09-08 01:38:08', '2018-09-08 01:38:08'),
(35, 'images/September2018/1KsaFpZB7vNhkqApU1CT.png', '8', '2018-09-08 01:38:23', '2018-09-08 01:38:23'),
(36, 'images/September2018/TX8NIvSpvVec7kSI4Hmd.png', '9', '2018-09-08 01:44:09', '2018-09-08 01:44:09'),
(37, 'images/September2018/T4AfJcgqnUAPJYZYJKgN.png', '9', '2018-09-08 01:44:25', '2018-09-08 01:44:25'),
(38, 'images/September2018/z9c8LyfHDz3HCkeOKF2K.png', '9', '2018-09-08 01:44:41', '2018-09-08 01:44:41'),
(40, 'images/September2018/uJX5yv2SPuOEnUEXcVqm.png', '9', '2018-09-08 01:45:20', '2018-09-08 01:45:20'),
(41, 'images/September2018/sUn0ECpy0JYhlGpIGus1.png', '10', '2018-09-08 02:55:39', '2018-09-08 02:55:39'),
(42, 'images/September2018/BlBRxpCDWg87s36zNNba.png', '10', '2018-09-08 02:56:04', '2018-09-08 02:56:04'),
(43, 'images/September2018/tt8K8ABlCNsYWARma28M.png', '10', '2018-09-08 02:56:26', '2018-09-08 02:56:26'),
(44, 'images/September2018/yGifODC6IrvCW7qjo3ws.png', '10', '2018-09-08 02:56:50', '2018-09-08 02:56:50'),
(45, 'images/December2018/gw6wLwvEKyPmzubs77Uv.png', '11', '2018-12-09 04:39:52', '2018-12-09 04:39:52'),
(46, 'images/December2018/0t8pPo87Hir9ogX4Jrtt.png', '11', '2018-12-09 04:40:47', '2018-12-09 04:40:47'),
(48, 'images/December2018/IWc1WAdH7IGvU7xGhwe8.png', '11', '2018-12-09 04:41:19', '2018-12-09 04:41:19'),
(49, 'images/December2018/gWYTE2Oqp4NyX0jRay7n.png', '11', '2018-12-09 04:41:42', '2018-12-09 04:41:42'),
(50, 'images/December2018/wQ701lgHNTIyVlzkqyjh.png', '12', '2018-12-13 22:09:00', '2018-12-13 22:09:13'),
(51, 'images/December2018/VWBo2cPaDjHaeZZ44lnN.png', '12', '2018-12-13 22:09:32', '2018-12-13 22:09:32'),
(52, 'images/December2018/BhIrX7ES8gxeUQrL3MZ0.png', '12', '2018-12-13 22:22:17', '2018-12-13 22:22:17'),
(53, 'images/September2020/Tn2rGBbkbqSGiTVN3nQd.png', '14', '2020-09-24 07:38:20', '2020-09-24 07:38:20'),
(54, 'images/September2020/cTHqfrlkjJoSiOqCBPHV.png', '14', '2020-09-24 07:38:33', '2020-09-24 07:38:33'),
(55, 'images/September2020/VOd6eRcavGIZ6spTffkC.png', '14', '2020-09-24 07:38:46', '2020-09-24 07:38:46');

-- --------------------------------------------------------

--
-- Table structure for table `menus`
--

CREATE TABLE `menus` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `menus`
--

INSERT INTO `menus` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'admin', '2018-08-26 00:53:57', '2018-08-26 00:53:57');

-- --------------------------------------------------------

--
-- Table structure for table `menu_items`
--

CREATE TABLE `menu_items` (
  `id` int(10) UNSIGNED NOT NULL,
  `menu_id` int(10) UNSIGNED DEFAULT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `target` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '_self',
  `icon_class` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `color` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `order` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `route` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parameters` text COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `menu_items`
--

INSERT INTO `menu_items` (`id`, `menu_id`, `title`, `url`, `target`, `icon_class`, `color`, `parent_id`, `order`, `created_at`, `updated_at`, `route`, `parameters`) VALUES
(1, 1, 'Dashboard', '', '_self', 'voyager-boat', NULL, NULL, 1, '2018-08-26 00:53:57', '2018-08-26 00:53:57', 'voyager.dashboard', NULL),
(2, 1, 'Media', '', '_self', 'voyager-images', NULL, NULL, 5, '2018-08-26 00:53:57', '2018-08-26 00:53:57', 'voyager.media.index', NULL),
(3, 1, 'Users', '', '_self', 'voyager-person', NULL, NULL, 3, '2018-08-26 00:53:57', '2018-08-26 00:53:57', 'voyager.users.index', NULL),
(4, 1, 'Roles', '', '_self', 'voyager-lock', NULL, NULL, 2, '2018-08-26 00:53:57', '2018-08-26 00:53:57', 'voyager.roles.index', NULL),
(5, 1, 'Tools', '', '_self', 'voyager-tools', NULL, NULL, 9, '2018-08-26 00:53:57', '2018-08-26 00:53:57', NULL, NULL),
(6, 1, 'Menu Builder', '', '_self', 'voyager-list', NULL, 5, 10, '2018-08-26 00:53:57', '2018-08-26 00:53:57', 'voyager.menus.index', NULL),
(7, 1, 'Database', '', '_self', 'voyager-data', NULL, 5, 11, '2018-08-26 00:53:57', '2018-08-26 00:53:57', 'voyager.database.index', NULL),
(8, 1, 'Compass', '', '_self', 'voyager-compass', NULL, 5, 12, '2018-08-26 00:53:57', '2018-08-26 00:53:57', 'voyager.compass.index', NULL),
(9, 1, 'BREAD', '', '_self', 'voyager-bread', NULL, 5, 13, '2018-08-26 00:53:57', '2018-08-26 00:53:57', 'voyager.bread.index', NULL),
(10, 1, 'Settings', '', '_self', 'voyager-settings', NULL, NULL, 14, '2018-08-26 00:53:57', '2018-08-26 00:53:57', 'voyager.settings.index', NULL),
(11, 1, 'Hooks', '', '_self', 'voyager-hook', NULL, 5, 13, '2018-08-26 00:53:57', '2018-08-26 00:53:57', 'voyager.hooks', NULL),
(12, 1, 'Partners', '', '_self', NULL, NULL, NULL, 15, '2018-08-27 00:39:04', '2018-08-27 00:39:04', 'voyager.partners.index', NULL),
(13, 1, 'Projects', '', '_self', NULL, NULL, NULL, 16, '2018-09-06 01:30:00', '2018-09-06 01:30:00', 'voyager.projects.index', NULL),
(14, 1, 'Images', '', '_self', NULL, NULL, NULL, 17, '2018-09-07 23:47:19', '2018-09-07 23:47:19', 'voyager.images.index', NULL),
(15, 1, 'Photos', '', '_self', NULL, NULL, NULL, 18, '2020-02-15 01:02:20', '2020-02-15 01:02:20', 'voyager.photos.index', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2016_01_01_000000_add_voyager_user_fields', 1),
(4, '2016_01_01_000000_create_data_types_table', 1),
(5, '2016_05_19_173453_create_menu_table', 1),
(6, '2016_10_21_190000_create_roles_table', 1),
(7, '2016_10_21_190000_create_settings_table', 1),
(8, '2016_11_30_135954_create_permission_table', 1),
(9, '2016_11_30_141208_create_permission_role_table', 1),
(10, '2016_12_26_201236_data_types__add__server_side', 1),
(11, '2017_01_13_000000_add_route_to_menu_items_table', 1),
(12, '2017_01_14_005015_create_translations_table', 1),
(13, '2017_01_15_000000_make_table_name_nullable_in_permissions_table', 1),
(14, '2017_03_06_000000_add_controller_to_data_types_table', 1),
(15, '2017_04_21_000000_add_order_to_data_rows_table', 1),
(16, '2017_07_05_210000_add_policyname_to_data_types_table', 1),
(17, '2017_08_05_000000_add_group_to_settings_table', 1),
(18, '2017_11_26_013050_add_user_role_relationship', 1),
(19, '2017_11_26_015000_create_user_roles_table', 1),
(20, '2018_03_11_000000_add_user_settings', 1),
(21, '2018_03_14_000000_add_details_to_data_types_table', 1),
(22, '2018_03_16_000000_make_settings_value_nullable', 1),
(23, '2018_05_22_203511_create_projects_table', 1),
(24, '2018_05_22_205426_create_partners_table', 1),
(25, '2018_05_22_214721_alter_partners_table_add_order_field', 1),
(26, '2018_09_07_183128_create_images_table', 2),
(27, '2020_02_14_183023_create_photos_table', 3);

-- --------------------------------------------------------

--
-- Table structure for table `partners`
--

CREATE TABLE `partners` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `site_url` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `order` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `partners`
--

INSERT INTO `partners` (`id`, `name`, `description`, `site_url`, `created_at`, `updated_at`, `order`) VALUES
(1, 'Caramba Moreno', 'http://carambamoreno.com/', 'http://carambamoreno.com/', '2018-09-06 01:26:00', '2018-10-24 21:36:43', 2),
(2, 'Linking Arts', 'http://www.linkingarts.com/', 'http://www.linkingarts.com/', '2018-09-06 01:26:00', '2018-10-24 21:36:50', 1),
(4, 'None', 'none', '#', '2018-09-06 01:29:00', '2018-10-24 21:34:50', 3);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` int(10) UNSIGNED NOT NULL,
  `key` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `table_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `key`, `table_name`, `created_at`, `updated_at`) VALUES
(1, 'browse_admin', NULL, '2018-08-26 00:53:57', '2018-08-26 00:53:57'),
(2, 'browse_bread', NULL, '2018-08-26 00:53:57', '2018-08-26 00:53:57'),
(3, 'browse_database', NULL, '2018-08-26 00:53:57', '2018-08-26 00:53:57'),
(4, 'browse_media', NULL, '2018-08-26 00:53:57', '2018-08-26 00:53:57'),
(5, 'browse_compass', NULL, '2018-08-26 00:53:57', '2018-08-26 00:53:57'),
(6, 'browse_menus', 'menus', '2018-08-26 00:53:57', '2018-08-26 00:53:57'),
(7, 'read_menus', 'menus', '2018-08-26 00:53:57', '2018-08-26 00:53:57'),
(8, 'edit_menus', 'menus', '2018-08-26 00:53:57', '2018-08-26 00:53:57'),
(9, 'add_menus', 'menus', '2018-08-26 00:53:57', '2018-08-26 00:53:57'),
(10, 'delete_menus', 'menus', '2018-08-26 00:53:57', '2018-08-26 00:53:57'),
(11, 'browse_roles', 'roles', '2018-08-26 00:53:57', '2018-08-26 00:53:57'),
(12, 'read_roles', 'roles', '2018-08-26 00:53:57', '2018-08-26 00:53:57'),
(13, 'edit_roles', 'roles', '2018-08-26 00:53:57', '2018-08-26 00:53:57'),
(14, 'add_roles', 'roles', '2018-08-26 00:53:57', '2018-08-26 00:53:57'),
(15, 'delete_roles', 'roles', '2018-08-26 00:53:57', '2018-08-26 00:53:57'),
(16, 'browse_users', 'users', '2018-08-26 00:53:57', '2018-08-26 00:53:57'),
(17, 'read_users', 'users', '2018-08-26 00:53:57', '2018-08-26 00:53:57'),
(18, 'edit_users', 'users', '2018-08-26 00:53:57', '2018-08-26 00:53:57'),
(19, 'add_users', 'users', '2018-08-26 00:53:57', '2018-08-26 00:53:57'),
(20, 'delete_users', 'users', '2018-08-26 00:53:57', '2018-08-26 00:53:57'),
(21, 'browse_settings', 'settings', '2018-08-26 00:53:57', '2018-08-26 00:53:57'),
(22, 'read_settings', 'settings', '2018-08-26 00:53:57', '2018-08-26 00:53:57'),
(23, 'edit_settings', 'settings', '2018-08-26 00:53:57', '2018-08-26 00:53:57'),
(24, 'add_settings', 'settings', '2018-08-26 00:53:57', '2018-08-26 00:53:57'),
(25, 'delete_settings', 'settings', '2018-08-26 00:53:57', '2018-08-26 00:53:57'),
(26, 'browse_hooks', NULL, '2018-08-26 00:53:57', '2018-08-26 00:53:57'),
(27, 'browse_partners', 'partners', '2018-08-27 00:39:04', '2018-08-27 00:39:04'),
(28, 'read_partners', 'partners', '2018-08-27 00:39:04', '2018-08-27 00:39:04'),
(29, 'edit_partners', 'partners', '2018-08-27 00:39:04', '2018-08-27 00:39:04'),
(30, 'add_partners', 'partners', '2018-08-27 00:39:04', '2018-08-27 00:39:04'),
(31, 'delete_partners', 'partners', '2018-08-27 00:39:04', '2018-08-27 00:39:04'),
(32, 'browse_projects', 'projects', '2018-09-06 01:29:59', '2018-09-06 01:29:59'),
(33, 'read_projects', 'projects', '2018-09-06 01:29:59', '2018-09-06 01:29:59'),
(34, 'edit_projects', 'projects', '2018-09-06 01:29:59', '2018-09-06 01:29:59'),
(35, 'add_projects', 'projects', '2018-09-06 01:29:59', '2018-09-06 01:29:59'),
(36, 'delete_projects', 'projects', '2018-09-06 01:29:59', '2018-09-06 01:29:59'),
(37, 'browse_images', 'images', '2018-09-07 23:47:19', '2018-09-07 23:47:19'),
(38, 'read_images', 'images', '2018-09-07 23:47:19', '2018-09-07 23:47:19'),
(39, 'edit_images', 'images', '2018-09-07 23:47:19', '2018-09-07 23:47:19'),
(40, 'add_images', 'images', '2018-09-07 23:47:19', '2018-09-07 23:47:19'),
(41, 'delete_images', 'images', '2018-09-07 23:47:19', '2018-09-07 23:47:19'),
(42, 'browse_photos', 'photos', '2020-02-15 01:02:20', '2020-02-15 01:02:20'),
(43, 'read_photos', 'photos', '2020-02-15 01:02:20', '2020-02-15 01:02:20'),
(44, 'edit_photos', 'photos', '2020-02-15 01:02:20', '2020-02-15 01:02:20'),
(45, 'add_photos', 'photos', '2020-02-15 01:02:20', '2020-02-15 01:02:20'),
(46, 'delete_photos', 'photos', '2020-02-15 01:02:20', '2020-02-15 01:02:20');

-- --------------------------------------------------------

--
-- Table structure for table `permission_role`
--

CREATE TABLE `permission_role` (
  `permission_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `permission_role`
--

INSERT INTO `permission_role` (`permission_id`, `role_id`) VALUES
(1, 1),
(2, 1),
(3, 1),
(4, 1),
(5, 1),
(6, 1),
(7, 1),
(8, 1),
(9, 1),
(10, 1),
(11, 1),
(12, 1),
(13, 1),
(14, 1),
(15, 1),
(16, 1),
(17, 1),
(18, 1),
(19, 1),
(20, 1),
(21, 1),
(22, 1),
(23, 1),
(24, 1),
(25, 1),
(26, 1),
(27, 1),
(28, 1),
(29, 1),
(30, 1),
(31, 1),
(32, 1),
(33, 1),
(34, 1),
(35, 1),
(36, 1),
(37, 1),
(38, 1),
(39, 1),
(40, 1),
(41, 1),
(42, 1),
(43, 1),
(44, 1),
(45, 1),
(46, 1);

-- --------------------------------------------------------

--
-- Table structure for table `photos`
--

CREATE TABLE `photos` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `short_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `url` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `photos`
--

INSERT INTO `photos` (`id`, `name`, `short_name`, `slug`, `description`, `url`, `created_at`, `updated_at`) VALUES
(1, NULL, NULL, NULL, NULL, 'photos/February2020/bPm7p8oX6y3E6nrTl6qT.jpeg', '2020-02-15 01:02:00', '2020-02-15 01:27:56'),
(2, NULL, NULL, NULL, NULL, 'photos/February2020/DJOrrTxr5xXNBexw2TGQ.jpeg', '2020-02-15 01:02:00', '2020-02-15 01:28:40'),
(3, NULL, NULL, NULL, NULL, 'photos/February2020/4zBoxYdj8DjtyoUhhxsh.jpeg', '2020-02-15 01:03:07', '2020-02-15 01:03:07'),
(4, NULL, NULL, NULL, NULL, 'photos/February2020/qW6xOvI8Q39Zdmgat734.jpeg', '2020-02-15 01:03:35', '2020-02-15 01:03:35'),
(5, NULL, NULL, NULL, NULL, 'photos/February2020/zVJrLW9D0S1Vsev7OgPW.jpeg', '2020-02-15 01:03:52', '2020-02-15 01:03:52'),
(7, NULL, NULL, NULL, NULL, 'photos/February2020/ofipIPDOTkbe4fEEUtXc.jpeg', '2020-02-15 01:04:17', '2020-02-15 01:04:17'),
(8, NULL, NULL, NULL, NULL, 'photos/February2020/VP4g2dayIL60wb65Q2pm.jpeg', '2020-02-15 01:04:35', '2020-02-15 01:04:35'),
(9, NULL, NULL, NULL, NULL, 'photos/February2020/HtBgAoE0QSNuP7NAc4GV.jpeg', '2020-02-15 01:04:52', '2020-02-15 01:04:52'),
(10, NULL, NULL, NULL, NULL, 'photos/February2020/bV95sJk03C6zB300CfZA.jpeg', '2020-02-15 01:05:05', '2020-02-15 01:05:05'),
(12, NULL, NULL, NULL, NULL, 'photos/February2020/OwXt2VhD3uFyRubK2PfE.jpg', '2020-02-15 01:12:05', '2020-02-15 01:12:05'),
(14, NULL, NULL, NULL, NULL, 'photos/February2020/YQC2BPDP87xTmjZaDh83.jpg', '2020-02-15 01:13:07', '2020-02-15 01:13:07'),
(15, NULL, NULL, NULL, NULL, 'photos/February2020/5ObD1kPPDL3uKHIeMiMJ.jpg', '2020-02-15 01:13:41', '2020-02-15 01:13:41'),
(16, NULL, NULL, NULL, NULL, 'photos/February2020/A96Fenxyyk04KGRyiHJd.jpg', '2020-02-15 01:14:12', '2020-02-15 01:14:12'),
(18, NULL, NULL, NULL, NULL, 'photos/March2020/xJfphGfcdBMjWrGUsAzd.jpg', '2020-03-09 07:04:11', '2020-03-09 07:04:11'),
(19, NULL, NULL, NULL, NULL, 'photos/March2020/Rwe9G0hiBnxa5YKrZgjW.jpg', '2020-03-09 07:05:34', '2020-03-09 07:05:34'),
(20, NULL, NULL, NULL, NULL, 'photos/March2020/M8CBtvMWGrpWE98RV3fh.jpg', '2020-03-09 07:06:13', '2020-03-09 07:06:13'),
(21, NULL, NULL, NULL, NULL, 'photos/March2020/cTCCOHDMMMK6p2afX8rg.jpg', '2020-03-09 07:07:04', '2020-03-09 07:07:04'),
(22, NULL, NULL, NULL, NULL, 'photos/March2020/0fKCVdNACynQEip5FXUm.jpg', '2020-03-09 07:07:51', '2020-03-09 07:07:51'),
(23, NULL, NULL, NULL, NULL, 'photos/March2020/4LYLXvGQHP3bKL8A3nEQ.jpg', '2020-03-09 07:08:36', '2020-03-09 07:08:36');

-- --------------------------------------------------------

--
-- Table structure for table `projects`
--

CREATE TABLE `projects` (
  `id` int(10) UNSIGNED NOT NULL,
  `partner_id` int(11) DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `short_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `url` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `thumbnail` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `video_url` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `projects`
--

INSERT INTO `projects` (`id`, `partner_id`, `name`, `short_name`, `slug`, `description`, `url`, `thumbnail`, `video_url`, `created_at`, `updated_at`) VALUES
(1, 4, 'QwkHire', 'QwkHire, a freelancer marketplace', 'qwkhire', 'QwkHire is a Laravel based web application with a growing user base. The platform connects Clients and Contractors, similar to Upwork but with some differences. My role here is to maintain existing code and lead the development process with a small team. I am the main developer in charge of developing and also testing new features. I helped to build the platform from the ground up to here and it has various API integrations and in some parts complex business logic, including an escrow system and a very powerful admin panel to manage everything on the application. This app has Laravel queues integrated to deal with the heavy processes, such as email and payment requests. For the front end we used a paid theme that uses bootstrap 3.7.', 'https://qwkhire.co.uk', 'projects/September2018/mAxXbUfVcBracWWLcfJD.png', 'https://www.youtube.com/embed/tt7gP_IW-1w', '2018-09-06 02:18:00', '2018-12-14 03:34:44'),
(3, 4, 'Sporta Gym', 'Sporta Gym, a custom site for a gym chain in Guatemala', 'sporta', 'Sporta.gt is a custom static website I created in 2016 for one of the biggest gyms here in Guatemala. What\'s interesting about this website is that I used no css framework for it. Rather I used my own css, most based on flexbox. This website has an integrated system with Aweber for automatic customer follow up depending on their current state (Active, Prospect, Inactive).', 'http://sporta.gt', 'projects/September2018/z0RrOwRrEOkFNdV9FxTj.png', 'https://www.youtube.com/embed/q2UUHb0Azg0', '2018-09-07 10:45:00', '2018-12-14 03:33:58'),
(4, 4, 'Modpart', 'Modpart, complex web app for business process optimization', 'modpart', 'Modpart was bootstrapped using Laravel Spark. Completely developed by me, the application features a powerful search for a large set of data, with multiple manual and automated filters that enhance the user experience and enable them to save a lot of time as opposed to following tedious manual processes. This web application uses Vue.js for the front end, which I love and try to use whenever I can in other projects. We use ElasticSearch for the search process along with Laravel Scout and a package driver for ES.\r\n\r\nNote: This site is currently under development, so no public content has been released.', 'http://modpart.com', 'projects/September2018/afLfmKqlKD06Xp4Qf0jI.png', 'https://www.youtube.com/embed/q2UUHb0Azg0', '2018-09-07 10:58:00', '2018-12-14 03:33:24'),
(5, 1, 'Alessa', 'Alessa, online shop for a big international jewelry brand', 'alessa', 'Alessa is a refined high end website, developed for a multi-millionaire company based in Guatemala, the US and Dubai. This is a jewelry store with prices up to $50,000 USD. My role here is to help build new features (mostly the backend) and also customize the online shop plugin accordingly so it meets the high end standards of the company. The admin panel is super powerful and has the ability to customize almost every single detail on the website, a good part of this was my job. My job also includes a smart product filter, maintenance to most of the site features including emails, login logic, newsletter systems, instagram integration and more. We are currently developing a custom solution to streamline CRM processes all the way from the store personnel.', 'https://alessa.jewelry', 'projects/September2018/RHXe1u71XNJvmlHsKgBf.png', 'https://www.youtube.com/embed/q2UUHb0Azg0', '2018-09-07 11:09:00', '2018-12-14 03:32:33'),
(6, 1, 'Yoli', 'Yoli, a fun and clean web application to print your instagram photos', 'yoli', 'Yoli is a cool web application that helps you get your instagram photos printed and delivered at your front door. The frame will be shipped to you with the design you picked. A Yoli Frame is also a perfect gift. My role in Yoli was to develop all the backend logic of the application including, user management, instagram api integration, frame backend modeling, etc. I also took care of the payment process.', 'https://yoli.gt', 'projects/September2018/xtf5cvzBUo362jlmp78c.png', 'https://www.youtube.com/embed/q2UUHb0Azg0', '2018-09-07 11:36:00', '2018-12-13 22:34:36'),
(7, 1, 'Guatephoto', 'Guatephoto, a charming website for a massive photography event in Guatemala', 'guatephoto', 'Guatephoto is a website to promote a big photography event that takes place in Guatemala City, the most important photographers in the region compete in different categories. My role here was purely backend and specifically on the payment and participant application logic. This is a simple yet charming website.', 'https://guatephoto.org', 'projects/September2018/2NSZLSGVNcEEnPnYq87l.png', 'https://www.youtube.com/embed/q2UUHb0Azg0', '2018-09-07 21:41:00', '2018-12-13 22:32:36'),
(8, 2, 'Ben-Iesau', 'Ben-Iesau.com, a portfolio site for a great artist based in NOLA', 'ben-iesau', 'Ben Iesau is a custom website created for Cheri Ben-Iesau (Artist). With this site the artist can customize almost everything on the website thanks to a CMS we built from scratch. The CMS is based on Laravel and was created thinking about how we could make the freelancer artist\'s life easier. Our system allows the user to customize each section of the website, push blog posts to all their social media platforms in one click (Facebook, Instagram, Twitter and Pinterest), import events from Facebook and it also has a menu builder. Whenever you upload an image the CMS automatically creates different versions of it for loading purposes. Our CMS also features a simple CRM tailored for artists.', 'https://ben-iesau.com', 'projects/September2018/WuDtEgCBO6hN2XuLDeZ5.png', 'https://www.youtube.com/embed/q2UUHb0Azg0', '2018-09-07 21:55:00', '2018-12-13 22:32:01'),
(9, 2, 'Pictureman', 'Pictureman, an amazing tool for photographers', 'pictureman', 'Pictureman is a system that enables a photographer to upload their event photos on-site and send them to their customers either through the website or the mobile application, the customer receives a nice formatted email with their photos. The system automatically builds an event page available for the public and posts the photos the photographer wants to showcase there. This system uses our social media blog posts system, so that you can push your blog posts automatically to all your social media platforms in one click. My role here was to create the image upload and optimization logic, social media blog posts and also a good part of the admin CRUD, among other smaller tasks.', 'https://pictureman.co', 'projects/September2018/YNwqkGmLkFj6UaksncOT.png', 'https://www.youtube.com/embed/q2UUHb0Azg0', '2018-09-07 22:11:00', '2018-12-12 00:57:08'),
(10, 2, 'Whereyart', 'Whereyart, an artists marketplace', 'whereyart', 'Whereyart is a Laravel based application that features artists based in New Orleans and coming soon other cities. My role here was to migrate from Laravel 4.1 to Laravel 5.6, I have also fixed a lot of bugs within a complex package they use for their CRUDL system. I suggested and implemented the usage of Laravel queues to optimize many of the application processes and now is a complete success, also the site speed increased a lot with the usage of L5.6 and PHP 7.2. I also created a home page banner creato, improved the join process and optimized load more functions across the site.', 'https://whereyart.net', 'projects/September2018/R4kwwjhFpQjru2IjBvQB.png', 'https://www.youtube.com/embed/q2UUHb0Azg0', '2018-09-07 22:22:00', '2018-12-12 00:56:02'),
(11, 4, 'IQ Builder Supply', 'IQ Builder Supply, a custom ecommerce website based on wordpress', 'iq-builder-supply', 'iqbuildersupply.com is a wordpress set up based on woocommerce and various premium extensions and plugins. The site allows users to order custom-size BiFolding doors, various beautiful Iron Doors, Tiles and Custom Cabinets. The site is a complete tool that will enable the owners to expand their business online seamlessly.', 'https://iqbuildersupply.com', 'projects/December2018/TXD9BKDHOJYtoX40nvi9.png', NULL, '2018-12-09 04:30:00', '2019-01-20 16:07:16'),
(12, 4, 'Nostra Gallery', 'Nostra Gallery, a marketplace for artists', 'nostra-gallery', 'Nostra Gallery is a fresh online store for artists mainly located in Guatemala. My role was to set up Wordpress and Woocommerce to bootstrap this clean store. Go checkout some of the art featured on the site!', 'https://nostragallery.com', 'projects/December2018/d3687Fqy8JNblF0ajHCf.png', NULL, '2018-12-13 22:08:00', '2020-09-24 07:42:44'),
(13, 4, 'Dungeon In A Box', 'Dungeon In A Box, a subscription based system with plenty of features', 'dungeon-in-a-box', 'Dungeon In a Box is a Wordpress and Woocommerce set up that uses Woocommerce Subscriptions as the base for the subscriptions system. The project has multiple custom features and adjustments to better fit the client\'s needs. Some of these features include:\r\n\r\n- Custom renewal synchronization of subscriptions with different billing intervals (for admins).\r\n- Custom theme.\r\n- Separation of billing with shipping orders (system creates an order using the customer\'s info and last product sent for shipping purposes only).\r\n\r\nAlso, a big feature developed for the site was a kickstarter pledge manager that allows users to select which product they want specifically after they pledged on a complex kickstarter campaign and set up. Prices change while customers select more, the tool allows users to add more than what they pledged for over kick-starter.', 'https://dungeoninabox.com', 'projects/March2020/wTxv5M08ubR8UroBwtBM.png', NULL, '2020-03-02 01:09:00', '2020-09-24 07:39:41'),
(14, 4, 'Bikebuilder App', 'Bikebuilder App, a custom web based application for internal use', 'bikebuilder-app', 'Bikebuilder is a completely custom web based application for internal use made for Mike\'s Bikes; one of the most important bicycle retailers in San Francisco, California. The application allows technicians to track repairs, including time, items used, etc. This has allowed the company to streamline a process that was previously done by paper and tedious excel spreadsheets. This project is the perfect example of how to use technology to solve problems efficiently.', NULL, 'projects/September2020/x6XXC9iwaKfcEGbLX8Sl.png', NULL, '2020-09-24 07:34:00', '2020-09-24 07:40:27');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `display_name`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'Administrator', '2018-08-26 00:53:57', '2018-08-26 00:53:57'),
(2, 'user', 'Normal User', '2018-08-26 00:53:57', '2018-08-26 00:53:57');

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` int(10) UNSIGNED NOT NULL,
  `key` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci,
  `details` text COLLATE utf8mb4_unicode_ci,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `order` int(11) NOT NULL DEFAULT '1',
  `group` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `key`, `display_name`, `value`, `details`, `type`, `order`, `group`) VALUES
(1, 'site.title', 'Site Title', 'Site Title', '', 'text', 1, 'Site'),
(2, 'site.description', 'Site Description', 'Site Description', '', 'text', 2, 'Site'),
(3, 'site.logo', 'Site Logo', '', '', 'image', 3, 'Site'),
(4, 'site.google_analytics_tracking_id', 'Google Analytics Tracking ID', '', '', 'text', 4, 'Site'),
(5, 'admin.bg_image', 'Admin Background Image', '', '', 'image', 5, 'Admin'),
(6, 'admin.title', 'Admin Title', 'Voyager', '', 'text', 1, 'Admin'),
(7, 'admin.description', 'Admin Description', 'Welcome to Voyager. The Missing Admin for Laravel', '', 'text', 2, 'Admin'),
(8, 'admin.loader', 'Admin Loader', '', '', 'image', 3, 'Admin'),
(9, 'admin.icon_image', 'Admin Icon Image', '', '', 'image', 4, 'Admin'),
(10, 'admin.google_analytics_client_id', 'Google Analytics Client ID (used for admin dashboard)', '', '', 'text', 1, 'Admin');

-- --------------------------------------------------------

--
-- Table structure for table `translations`
--

CREATE TABLE `translations` (
  `id` int(10) UNSIGNED NOT NULL,
  `table_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `column_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `foreign_key` int(10) UNSIGNED NOT NULL,
  `locale` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `avatar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT 'users/default.png',
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `settings` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `role_id`, `name`, `email`, `avatar`, `password`, `remember_token`, `settings`, `created_at`, `updated_at`) VALUES
(1, 1, 'diego', 'jay.dee.force@gmail.com', 'users/August2018/xgXNOQdKMQwizm3WvFjt.jpg', '$2y$10$YiYXs4pXPSRjKdGMHhniaecq2Z2TD19egAfYV6PeKBQ3e9PUs5Ef2', 'tN2pXkKlJFLFAgT0jYyBVDWBPSGRQUrfW9YYzUAVGmrzZF4I0FmM2pgjQIKK', '{\"locale\":\"en\"}', '2018-08-26 00:54:38', '2018-08-27 00:36:26');

-- --------------------------------------------------------

--
-- Table structure for table `user_roles`
--

CREATE TABLE `user_roles` (
  `user_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `data_rows`
--
ALTER TABLE `data_rows`
  ADD PRIMARY KEY (`id`),
  ADD KEY `data_rows_data_type_id_foreign` (`data_type_id`);

--
-- Indexes for table `data_types`
--
ALTER TABLE `data_types`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `data_types_name_unique` (`name`),
  ADD UNIQUE KEY `data_types_slug_unique` (`slug`);

--
-- Indexes for table `images`
--
ALTER TABLE `images`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `menus`
--
ALTER TABLE `menus`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `menus_name_unique` (`name`);

--
-- Indexes for table `menu_items`
--
ALTER TABLE `menu_items`
  ADD PRIMARY KEY (`id`),
  ADD KEY `menu_items_menu_id_foreign` (`menu_id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `partners`
--
ALTER TABLE `partners`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `permissions_key_index` (`key`);

--
-- Indexes for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `permission_role_permission_id_index` (`permission_id`),
  ADD KEY `permission_role_role_id_index` (`role_id`);

--
-- Indexes for table `photos`
--
ALTER TABLE `photos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `projects`
--
ALTER TABLE `projects`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `roles_name_unique` (`name`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `settings_key_unique` (`key`);

--
-- Indexes for table `translations`
--
ALTER TABLE `translations`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `translations_table_name_column_name_foreign_key_locale_unique` (`table_name`,`column_name`,`foreign_key`,`locale`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD KEY `users_role_id_foreign` (`role_id`);

--
-- Indexes for table `user_roles`
--
ALTER TABLE `user_roles`
  ADD PRIMARY KEY (`user_id`,`role_id`),
  ADD KEY `user_roles_user_id_index` (`user_id`),
  ADD KEY `user_roles_role_id_index` (`role_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `data_rows`
--
ALTER TABLE `data_rows`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=59;

--
-- AUTO_INCREMENT for table `data_types`
--
ALTER TABLE `data_types`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `images`
--
ALTER TABLE `images`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=56;

--
-- AUTO_INCREMENT for table `menus`
--
ALTER TABLE `menus`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `menu_items`
--
ALTER TABLE `menu_items`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `partners`
--
ALTER TABLE `partners`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=47;

--
-- AUTO_INCREMENT for table `photos`
--
ALTER TABLE `photos`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `projects`
--
ALTER TABLE `projects`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `translations`
--
ALTER TABLE `translations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `data_rows`
--
ALTER TABLE `data_rows`
  ADD CONSTRAINT `data_rows_data_type_id_foreign` FOREIGN KEY (`data_type_id`) REFERENCES `data_types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `menu_items`
--
ALTER TABLE `menu_items`
  ADD CONSTRAINT `menu_items_menu_id_foreign` FOREIGN KEY (`menu_id`) REFERENCES `menus` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`);

--
-- Constraints for table `user_roles`
--
ALTER TABLE `user_roles`
  ADD CONSTRAINT `user_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `user_roles_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
