<?php

namespace App\Http\Controllers;

use App\Partner;
use App\Project;
use App\Photo;

class PublicPagesController extends Controller
{
    public function home(){
    
        return view('pages.home', ['name' => 'home']);
        
    }

    public function portfolio(){

        return view('pages.portfolio', [
        	'name' => 'portfolio',
        	// 'projects' => Project::with('partner')->get(),
        	'partners' => Partner::orderBy('order')->get(),
        ]);
        
    }

    public function resume(){
    
        return view('pages.resume', ['name' => 'resume']);
        
    }

    public function services(){
    
        return view('pages.services', ['name' => 'services']);
        
    }

    public function modeling(){

        return view('pages.modeling', [
            'name' => 'modeling',
            'photos' => Photo::all()
        ]);
    
    }
}
