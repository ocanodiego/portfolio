<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{

    const STATUS_DRAFT = 0;
    const STATUS_PUBLISHED = 1;

    protected $guarded = ['id'];

    protected $statusLabels = [
        self::STATUS_DRAFT => 'Draft',
        self::STATUS_PUBLISHED => 'Published'
    ];

    public function getStatusAttribute(){
        return self::statusLabels[$this->status_id]; 
    }

    public function getNakedUrlAttribute(){
    
    	return preg_replace("#^http(s)?://#", '', $this->internal_url);
    	
    }

    public function getPublicThumbnailAttribute(){
    
    	return "https://poncianodiego.com/storage/" . $this->attributes['thumbnail'];
    	
    }

    public function getInternalUrlAttribute(){
        return route('project.show', $this->slug);
    }

    public function partner(){
    
    	return $this->belongsTo(Partner::class);
    	
    }

    public function images(){
        return $this->hasMany(Image::class, 'relation_id');
    }

    public function scopePublished($query){
        return $query->where('status_id', self::STATUS_PUBLISHED);
    }
}
