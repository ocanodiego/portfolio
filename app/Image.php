<?php

namespace App;

use App\Project;
use Illuminate\Database\Eloquent\Model;

class Image extends Model
{

    protected $guarded = ['id'];

    public function project(){
    	return $this->belongsTo(Project::class, 'relation_id');
    }

    public function getPublicUrlAttribute(){
    	return "https://poncianodiego.com/storage/" . $this->attributes['url'];
    }
}
